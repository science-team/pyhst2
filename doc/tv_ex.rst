Total Variation
==============

You find the the example in ::

/scisoft/ESRF_sw/TEST_PYHST/TV_PHANTOM


.. image:: datas/examples/totalvariation/slice_bp.png
    :width:  300
    :height: 300
.. image:: datas/examples/totalvariation/slice_nocorr.png
    :width:  300
    :height: 300

Input File:

.. literalinclude::  datas/examples/totalvariation/input_bp.par
    :language: python



.. image:: datas/examples/totalvariation/slice_bp.png
    :width:  300
    :height: 300

Input File:

.. literalinclude::  datas/examples/totalvariation/input_fbp.par
    :language: python

