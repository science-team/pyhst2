Iterative Corrections
=====================
Iterative corrections work with parallel or fan geometry.
Not yet with helical scans and not with conical geometry. 
 
Contents:

.. toctree::
   :maxdepth: 3

   total_variation
   patches
   continuous_scan_integration
   nn_fbp


#    nlm

