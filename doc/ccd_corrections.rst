CCD Corrections
=====================


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DO_CCD_FILTER, CCD_FILTER, CCD_FILTER_PARA
	       
 
