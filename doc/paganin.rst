Paganin Filtering
=====================


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DO_PAGANIN,PAGANIN_Lmicron,PAGANIN_MARGE,PUS,PUC,UNSHARP_LOG,DO_OUTPUT_PAGANIN,OUTPUT_PAGANIN_FILE
	       
 
