.. _tutorial-nonregression:

Non Regression Tests
====================


  * get the datasets archive from http://ftp.esrf.fr/scisoft/PYHST2/TEST_PYHST2.tgz  and untar it somewhere. Let's say that you copy the tar file in directory named /DATA and you untar it there.
  * Now go into directory TEST_PYHST and edit the script non regression.py
  You have to edit three variables there :
    * LAUNCHING_INSTRUCTION
    * outputprefix
    * PREFIX
    * casi
  
The variable LAUNCHING_INSTRUCTION determine how the program is launche.
The following setup will launch one process of the PyHST2_2016c version
and it will use GPU number 0 of the host sciosft12, this suppose that you are on scisoft12. Change the line according to you machine and to your pyhst version ::

  LAUNCHING_INSTRUCTION  = "echo 'localhost\n' > machinefile     ; time PyHST2_2016a  input.par   scisoft12,0"

The following instruction determine where the results will be written ::

    outputprefix="/data/scisofttmp/mirone/TEST_PYHST/RESULTS/2017c/tests"

Where datasets can be found is given by PREFIX, assuming that you have untarred the data in /DATA ::
  
   PREFIX="/DATA/DATASETS/"

finally by variable *casi* you can select which  cases will be run.
By default all the one distributed with  the data archive will
be runned.

Finally you can launch the tests ::

  python nonregressions.py




