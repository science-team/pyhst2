
typedef  struct {
  int V3D_TIFFREAD ;
  int V3D_TIFFOUTPUT ;
  int   V3D_CONVZ ;
  char *V3D_FILENAMEPATTERN     ;
  char *V3D_OUTPUTFILENAMEPATTERN;
  int *V3D_VOLDIMS   ;
  int *V3D_ROICORNER ;
  int *V3D_ROISIZE   ;
  float V3D_PUS ;
  float V3D_PUC ;
  int   V3D_GPUNUM ;
}  Unsharp_Para ;
  

#ifdef __cplusplus
extern "C" {
  int mainunsharp( Unsharp_Para  unsharp_para  );
}
#endif
