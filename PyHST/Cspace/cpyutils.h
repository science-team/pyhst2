
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
PyObject *  cpyutils_get_pydic_Oval(PyObject *Odict, const char *key ) ;
// int cpyutils_o2i(PyObject *obj );
int   cpyutils_o2i_TB(PyObject *obj , const   char * callFILE, int callLINE );
float cpyutils_o2f_TB(PyObject *obj , const char * callFILE, int callLINE );
// float cpyutils_o2f(PyObject *obj );
PyObject *cpyutils_getattribute(PyObject * obj , const char * string ) ;
char** cpyutils_getstringlist_from_list(PyObject *Otmp,int *listlenght) ;
void *cpyutils_PyArray1D_as_array(PyObject *Otmp, int *dim , int  pyarraytype );
void  *cpyutils_PyArray1D_as_array_TB(PyObject *OtmpA, int *listlenght,int  pyarraytype, const char * callFILE, int callLINE  );

void *cpyutils_PyArray2D_as_array(PyObject *Otmp, int *dim2, int *dim1,int  pyarraytype );
void *cpyutils_PyArray3D_as_array(PyObject *Otmp, int *dim3,int *dim2, int *dim1,int  pyarraytype );

void *cpyutils_PyArray5D_as_array(PyObject *Otmp,int *dim5,
				  int *dim4, int *dim3,
				  int *dim2, int *dim1,int  pyarraytype );



char* cpyutils_getstring(PyObject *Otmp) ;

void extended_fread(   char *ptr,      /* memory to write in */
                       int   size_of_block,
                       int ndims        ,
                       int *dims      ,
                       int  *strides    ,
                       FILE *stream  ) ;
