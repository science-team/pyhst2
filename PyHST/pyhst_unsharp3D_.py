
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


from __future__ import absolute_import
from __future__ import print_function
from __future__ import division


import os
import numpy as np

import Parameters_module

ncpus  =  Parameters_module.ncpus
myrank=0
print( " process " , myrank, " mygpus = " , Parameters_module.mygpus)

if Parameters_module.mygpus!=[]:
    Parameters_module.Parameters.MYGPU=  Parameters_module.mygpus[0]
else:
    Parameters_module.Parameters.MYGPU= -1


assert(  Parameters_module.Parameters.MYGPU>=0    )

dirname=os.path.dirname(os.path.abspath(__file__))

import unsharp3D


P=Parameters_module.Parameters
P.V3D_VOLDIMS = np.array(  P.V3D_VOLDIMS ,"i")
P.V3D_ROICORNER = np.array(  P.V3D_ROICORNER ,"i")
P.V3D_ROISIZE = np.array(  P.V3D_ROISIZE,"i" )

unsharp3D.unsharp3D(   Parameters_module.Parameters )


 
