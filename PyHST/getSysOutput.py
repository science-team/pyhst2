
#/*##########################################################################
# Copyright (C) 2001-2013 European Synchrotron Radiation Facility
#
#              PyHST2  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# PyHST2 is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for PyHST2: Alessandro Mirone.
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PyHST2 is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PyHST2; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PyHST2 follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import string
import sys
import os
import select
HUGE = 10000 

def getSysOutput(command):
    rd, wr = os.pipe()   # Create the pipe
    pid = os.fork()   # Create a new process
    if  pid:    # Parent code
     print( "MYPID ", pid)
     os.close(wr)   # The parent won't write the pipe
     fdlist=[rd]

     msgtot=""
     while fdlist != []:
         (readable, writeable, oob) = select.select(fdlist, [], [])
         # writeable and oob will always be '[]' here, since we're
         # not waiting for them
         for f in readable:
          msg=os.read(f, HUGE)
          if msg != '':
           if msg[-1:] == '\n': # This is always the case
            msg = msg[:-1] # Strip the newline
            msgtot=msgtot+msg
          else:   # Read zero bytes => child terminated
              os.close(f)
              fdlist=[]
              break
    else:
        os.close(rd)   # The child won't read the pipe
        # Make sure the child won't read input intended for the parent:
        null = open('/dev/null')
        os.dup2(null.fileno(), sys.stdin.fileno())
        null.close()
        os.dup2(wr, sys.stdout.fileno())
        os.system(command )
        os.close(wr)
        sys.exit(0)   # Terminate the child process

    return msgtot
