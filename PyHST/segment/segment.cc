#include<stdio.h>
#include<omp.h>
#include<string.h>
#include<stdlib.h>


#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>

#include <unistd.h>




#define min(a,b) (((a)<(b)) ? (a):(b))
#define max(a,b) (((a)>(b)) ? (a):(b))

#define BLOCKSIZE 128

#ifndef O_LARGEFILE
#define O_LARGEFILE 0
#endif

int my_read(char * file_name, void *buf, size_t count , off_t pos_in_file) {
  int fd;
  fd = open( file_name, O_RDONLY|O_LARGEFILE);
  if( fd == -1 ) {
    printf("Could not open file %s now exiting\n", file_name);
    fprintf(stderr, "Could not open file %s now exiting\n", file_name);
    exit(1);
  }
  lseek(fd, pos_in_file, SEEK_SET);
  off_t chunk=0;
  while( chunk < count) {
    size_t letti;
    size_t toberead = min( count - chunk, 100000000 );
    letti = read( fd, ((char*)buf)+chunk, toberead );
    if( letti<0) {
      printf("Could not completely read the required chunk from file  %s now exiting\n", file_name);
      fprintf(stderr, "Could not completely read the required chunk from file %s now exiting\n", file_name);
      exit(1);

    }
    chunk = chunk + letti;
    //  printf(" LETTURA %ld %ld\n", chunk, count);
  }
  close(fd);
  return 1;
}
int my_write(char * file_name, void *buf, size_t count , off_t pos_in_file) {
  struct flock fl;
  int fd;
  fd = open( file_name,   O_RDWR | O_CREAT |O_LARGEFILE , 640);
  if( fd == -1 ) {
    printf("Could not open file %s for writing now exiting\n", file_name);
    fprintf(stderr, "Could not open file %s for writing now exiting\n", file_name);
    exit(1);
  }
  fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
  fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */

  fl.l_len    = count; /* length, 0 = to EOF           */
  fl.l_pid    = getpid(); /* our PID                      */

  fl.l_start  =  ((long int) pos_in_file); 
  fcntl(fd, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
  lseek( fd  ,pos_in_file, SEEK_SET);

  off_t chunk=0;
  while( chunk < count) {
    size_t scritti;
    size_t tobewritten = min( count - chunk, 100000000 );
    scritti = write( fd, ((char*)buf)+chunk,  tobewritten);
    if( scritti<0) {
      printf("Could not completely write the required chunk to file  %s now exiting\n", file_name);
      fprintf(stderr, "Could not completely write the required chunk to file %s now exiting\n", file_name);
      exit(1);

    }
    chunk = chunk + scritti;
    //  printf(" LETTURA %ld %ld\n", chunk, count);
  }
  fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
  fcntl(fd, F_SETLK, &fl); /* set the region to unlocked */
  close(fd);
  return 1;
}


void  segment( char *pagbone_outputfile,
	       float threshold,
	       int medianR,
	       char *maskbone_outputfile,
	       int myrank,
	       int nprocs,
	       int ncpus,
	       long int MemPerProc, long int SIZEZ, long int SIZEY, long int SIZEX, int dilatation ) {

  printf(" in c++ %ld %ld %ld ncpus %d \n", SIZEZ,  SIZEY, SIZEX, ncpus);

  long int margin = dilatation + medianR ;


  long int zPstep =  ((SIZEZ+nprocs-1)/nprocs);
  long int Pstart =   zPstep*myrank   ;
  long int Pend   =   min( Pstart + zPstep, SIZEZ   )        ;
  long int zTstep =  (((Pend-Pstart)+ncpus-1)/ncpus);

 #define NZs 20

  long int required_mem_per_thread =   2*(NZs+2*margin)*BLOCKSIZE*BLOCKSIZE *sizeof(int)  + sizeof(float)*(2*NZs+2*margin)*SIZEY*SIZEX   ;
  int tot_cpus = ncpus;
  if(  required_mem_per_thread * ncpus * nprocs >  MemPerProc * ncpus *nprocs) {
    while( ncpus>0) {
      if (   required_mem_per_thread * ncpus * nprocs >  0.9*MemPerProc * tot_cpus *nprocs  ) {
	fprintf(stdout,"Not enough memory in routine segment. Now reducing number of threads. Possibly think to reduce the slab size(hardcoded) or increase the total available memory \n");
	ncpus = ncpus -1 ; 
      } else {
	break;
      }
    }
    if( ncpus == 0 ) {
      fprintf(stderr,"Not enough memory in routine segment. Think to reduce the slab size(hardcoded) or increase the total available memory\n");
      exit(1);
    }
  }
  printf(" ncpus %d \n", ncpus);
  
  omp_set_num_threads(ncpus);
#pragma omp  parallel
  {
    int ID = omp_get_thread_num();
    //printf(" ID %d \n", ID);
    long int Tstart =   Pstart + zTstep*ID   ;
    long int Tend   =   min( Tstart + zTstep , Pend   )        ;

    long int istart, iend;



    int *   Rblock = (int*) malloc(  (NZs+2*margin)*BLOCKSIZE*BLOCKSIZE *sizeof(int));
    int *   Mblock = (int*) malloc(  (NZs+2*margin)*BLOCKSIZE*BLOCKSIZE *sizeof(int));
    float *input_data  = (float *) malloc(sizeof(float)*(NZs+2*margin)*SIZEY*SIZEX   );
    float *output_data = (float *) malloc(sizeof(float)* NZs *SIZEY*SIZEX   );

    for(istart = Tstart; istart<Tend; istart+=NZs) {
      printf(" ID %d istart %ld \n", ID, istart);
      iend = min(istart +NZs, Tend);
      long int Rstart = max(istart-margin,0);
      long int Rend   = min(iend  +margin,SIZEZ);
      
 
      long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;
      my_read(   pagbone_outputfile  ,
		input_data          ,
		(size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		(off_t) l_start
		);

      
      int  falso = 0 ;

      long int DY = BLOCKSIZE-2*margin;
      long int DX = BLOCKSIZE-2*margin;

      for(long int iY = 0 ; iY < SIZEY; iY+=DY) {
	long int eY = min(iY+DY, SIZEY);
	for(long int iX = 0 ; iX < SIZEX; iX+=DX) {
	  long int eX = min(iX+DX, SIZEX);

	  // printf(" ID %d costruisce blocco iY iX %d %d \n", ID, iY, iX);

	  for(long int rz=0; rz<NZs+2*margin; rz++) {
	    long int Z = rz + istart -margin;
	    if( Z >= Rstart &&    Z<Rend) {
	      Z = Z - Rstart ; 
	      for(long int ry=0; ry<BLOCKSIZE; ry++) {
		long int Y = ry + iY -margin ; 
		if( Y>=0 && Y < SIZEY) {
		  for(long int rx=0; rx<BLOCKSIZE; rx++) {
		    long int X = rx + iX -margin ; 
		    int  rdata ; 
		    if( X>=0 && X < SIZEX) {
		      rdata  = ( input_data[( Z* SIZEY  + Y ) *SIZEX  + X ] > threshold );
		    } else {
		      rdata = falso ;
		    }
		    Rblock[( rz* BLOCKSIZE + ry ) *  BLOCKSIZE+ rx  ]  = rdata;
		  }
		} else {
		  for(long int rx=0; rx<BLOCKSIZE; rx++)  Rblock[( rz*BLOCKSIZE   + ry ) * BLOCKSIZE + rx  ]  = falso ;
		}
	      }
	    } else {
	      for(long int ry=0; ry<BLOCKSIZE; ry++) for(long int rx=0; rx<BLOCKSIZE; rx++)  Rblock[( rz*BLOCKSIZE   + ry ) * BLOCKSIZE + rx  ]  = falso   ;
	    }
	  }
	  
	  int *A=NULL, *B=NULL;
	  if( medianR) {
	    long int mediano = ( (2*medianR+1)*  (2*medianR+1)*  (2*medianR+1)      )/2;
	    A = Rblock;
	    B = Mblock ; 
	    for(long int z = medianR; z< NZs+2*margin-medianR; z++) {
	      for(long int y=0; y< BLOCKSIZE; y++) {
		for(long int x=0; x< BLOCKSIZE; x++) {
		  long int sum=0;
		  for(long int s = -medianR; s<=medianR; s++) {
		    sum +=  A[ ( (z+s)*BLOCKSIZE + (y+0))*BLOCKSIZE + (x+0) ]  ; 
		  }
		  B[ ( z*BLOCKSIZE + y)*BLOCKSIZE + x ] = sum;
		}
	      }
	    }
	    B = Rblock;
	    A = Mblock ; 
	    for(long int z = medianR; z< NZs+2*margin-medianR; z++) {
	      for(long int y=medianR; y< BLOCKSIZE-medianR; y++) {
		for(long int x=0; x< BLOCKSIZE; x++) {
		  long int sum=0;
		  for(long int s = -medianR; s<=medianR; s++) {
		    sum +=  A[ ( (z+0)*BLOCKSIZE + (y+s))*BLOCKSIZE + (x+0) ]  ; 
		  }
		  B[ ( z*BLOCKSIZE + y)*BLOCKSIZE + x ] = sum;
		}
	      }
	    }
	    A = Rblock;
	    B = Mblock ; 
	    for(long int z = medianR; z< NZs+2*margin-medianR; z++) {
	      for(long int y=medianR; y< BLOCKSIZE-medianR; y++) {
		for(long int x=medianR; x< BLOCKSIZE-medianR; x++) {
		  long int sum=0;
		  for(long int s = -medianR; s<=medianR; s++) {
		    sum +=  A[ ( (z+0)*BLOCKSIZE + (y+0))*BLOCKSIZE + (x+s) ]  ; 
		  }
		  B[ ( z*BLOCKSIZE + y)*BLOCKSIZE + x ] = (sum >= mediano);
		}
	      }
	    }
	  } else {
	    B = Rblock;
	    A = Mblock;
	  }

	  for(long int z = istart-dilatation ; z<istart+NZs+dilatation; z++) {
	    for(long int y=iY-dilatation; y<eY+dilatation; y++) {
	      for(long int x=iX-dilatation; x<eX+dilatation; x++) {
		long int posinblock =   (((z-istart)+margin)*BLOCKSIZE + ((y-iY) +margin  ))*BLOCKSIZE + ((x-iX) +margin  ) ; 
		if(B[posinblock])  B[posinblock]=0 ;
		else               B[posinblock]=-1 ;
	      }
	    }
	  }
	  
#define NOMASK 100000


	  for(long int z = istart; z<istart+NZs; z++) {
	    for(long int y=iY-dilatation; y<eY+dilatation; y++) {
	      for(long int x=iX-dilatation; x<eX+dilatation; x++) {
		float mask= NOMASK;
		long int sx=0, sy=0;
		for(long int sz = -dilatation ; sz<dilatation+1; sz++) {
		  long int posinblock =   (((z-istart)+margin+sz)*BLOCKSIZE + ((y-iY) +margin +sy ))*BLOCKSIZE + ((x-iX) +margin +sx ) ; 
		  if(B[posinblock]>=0) mask = min(mask, sz*sz ) ;
		}

		if(mask==NOMASK) mask = -1 ; 
		
		A[(((z-istart)+margin)*BLOCKSIZE + ((y-iY) +margin))*BLOCKSIZE + ((x-iX) +margin)] = mask ; 


	      }
	    }
	  }
	  for(long int z = istart; z<istart+NZs; z++) {
	    for(long int y=iY; y<eY; y++) {
	      for(long int x=iX-dilatation; x<eX+dilatation; x++) {
		float mask=NOMASK;
		long int sx=0, sz=0;
		for(long int sy = -dilatation ; sy<dilatation+1; sy++) {
		  long int posinblock =   (((z-istart)+margin+sz)*BLOCKSIZE + ((y-iY) +margin +sy ))*BLOCKSIZE + ((x-iX) +margin +sx ) ; 
		  if(A[posinblock]>=0) mask = min(mask,A[posinblock]+sy*sy   )  ;
		}
		if(mask==NOMASK) mask = -1 ; 
		B[(((z-istart)+margin)*BLOCKSIZE + ((y-iY) +margin))*BLOCKSIZE + ((x-iX) +margin)] = mask ; 
	      }
	    }
	  }
	  for(long int z = istart; z<istart+NZs; z++) {
	    for(long int y=iY; y<eY; y++) {
	      for(long int x=iX; x<eX; x++) {
		float mask=NOMASK;
		long int sy=0, sz=0;
		for(long int sx = -dilatation ; sx<dilatation+1; sx++) {
		  long int posinblock =   (((z-istart)+margin+sz)*BLOCKSIZE + ((y-iY) +margin +sy ))*BLOCKSIZE + ((x-iX) +margin +sx ) ; 
		  if(B[posinblock]>=0) mask = min(mask,B[posinblock]+sx*sx)   ;
		}
		if(mask==NOMASK) {
		  mask = 0 ; 
		} else {
		  if(mask>dilatation*dilatation+0.01) {
		    mask=0;
		  } else {
		    mask = 1-mask/dilatation/dilatation/3;
		  }
		  // mask = sqrt(mask);
		  // if(mask>dilatation+0.01) mask=0;
		  // else mask = mask = 1.0 - mask / (dilatation+1);
		}
		output_data[( (z-istart)* SIZEY  + y ) *SIZEX  + x ] = mask ; 
	      }
	    }
	  }
	  // printf(" ID %d costruito blocco iY iX %d %d \n", ID, iY, iX);
	  
	  // for(int z = istart; z<istart+NZs; z++) {
	  //   for(int y=iY; y<eY; y++) {
	  //     for(int x=iX; x<eX; x++) {
	  // 	int mask=0;
	  // 	for(int sz = -dilatation ; sz<dilatation+1; sz++) {
	  // 	  for(int sy = -dilatation ; sy<dilatation+1; sy++) {
	  // 	    for(int sx = -dilatation ; sx<dilatation+1; sx++) {
	  // 	      int posinblock =   (((z-istart)+margin+sz)*BLOCKSIZE + ((y-iY) +margin +sy ))*BLOCKSIZE + ((x-iX) +margin +sx ) ; 
	  // 	      if(B[posinblock]) mask = 1;
	  // 	    }
	  // 	  }
	  // 	}
	  // 	output_data[( (z-istart)* SIZEY  + y ) *SIZEX  + x ] = mask ; 
	  //     }
	  //   }
	  // }
	}
      }
      // printf(" scrivo pezzo su file \n");
      if(0) {
	struct flock fl;
	int fd;
	
	fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */

	fl.l_len    = sizeof(float)*(Rend-Rstart)*SIZEY*SIZEX; /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */


	fd = open( maskbone_outputfile , O_RDWR | O_CREAT , 640);
	if(fd==-1) {	
	  fprintf(stderr, " ERROR : could not open : %s  \n", maskbone_outputfile );
	  exit(0);
	}

	// printf(" locking slices %d to %d   in file %s \n",istart, iend,  maskbone_outputfile );
	fl.l_start  =  ((long int) istart)*sizeof(float)*SIZEY*SIZEX; 
	fcntl(fd, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
	lseek( fd  ,fl.l_start , SEEK_SET);
	write(fd, output_data  ,   sizeof(float)* (iend-istart)*SIZEY*SIZEX);
	fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
	fcntl(fd, F_SETLK, &fl); /* set the region to unlocked */
	// printf("unlocked \n");
	close(fd);
      } else {
	my_write(maskbone_outputfile,
		 output_data,
		 (size_t) sizeof(float)* (iend-istart)*SIZEY*SIZEX,
 		 (off_t) ((long int) istart)*sizeof(float)*SIZEY*SIZEX ) ;
	

      }

    }
    free(input_data);
    free(output_data);
    free(Rblock);
    free(Mblock);
  }
}







void  spilla ( char *pagbone_outputfile,
	       char *absbone_outputfile,
	       char *maskbone_outputfile,
	       float threshold,
	       char *corrbone_outputfile,
	       int myrank,
	       int nprocs,
	       int ncpus,
	       long int MemPerProc, long int SIZEZ, long int SIZEY, long int SIZEX) {

  printf(" in spilla %ld %ld %ld ncpus %d \n", SIZEZ,  SIZEY, SIZEX, ncpus);


  long int margin = 20;

  
  long int required_mem_per_thread = 4*sizeof(float)*(NZs+2*margin)*SIZEY*SIZEX   ;
  int tot_cpus = ncpus;
  if(  required_mem_per_thread * ncpus * nprocs >  MemPerProc * ncpus *nprocs) {

    while( ncpus>0) {
      if (   required_mem_per_thread * ncpus * nprocs >  0.9*MemPerProc * tot_cpus *nprocs  ) {
	fprintf(stdout,"Not enough memory in routine spilla. Now reducing number of threads.Possibly think to reduce the slab size(hardcoded) or increase the total memort \n");
	ncpus = ncpus -1 ; 
      } else {
	break;
      }
    }
    if( ncpus == 0 ) {
      fprintf(stderr,"Not enough memory in routine spilla. Think to reduce the slab size or increase the total memory\n");
      exit(1);
    }
  }
  printf(" ncpus %d \n", ncpus);

  long int zPstep =  ((SIZEZ+nprocs-1)/nprocs);
  long int Pstart =   zPstep*myrank   ;
  long int Pend   =   min( Pstart + zPstep, SIZEZ   )        ;
  long int zTstep =  (((Pend-Pstart)+ncpus-1)/ncpus);

   omp_lock_t writelock;
   omp_init_lock(&writelock);
   omp_lock_t readlock_1;
   omp_init_lock(&readlock_1);
   omp_lock_t readlock_2;
   omp_init_lock(&readlock_2);
   omp_lock_t readlock_3;
   omp_init_lock(&readlock_3);


  
  omp_set_num_threads(ncpus);
#pragma omp  parallel
  {
    long int ID = omp_get_thread_num();
    long int Tstart =   Pstart + zTstep*ID   ;
    long int Tend   =   min( Tstart + zTstep , Pend   )        ;

    long int istart, iend;


 
    float *pag_data    = (float *) malloc(sizeof(float)*(NZs+2*margin)*SIZEY*SIZEX   );
    float *abs_data    = (float *) malloc(sizeof(float)*(NZs+2*margin)*SIZEY*SIZEX   );
    float *mask_data   = (float *) malloc(sizeof(float)*(NZs+2*margin)*SIZEY*SIZEX   );
    float *output_data = (float *) malloc(sizeof(float)* (NZs+2*margin) *SIZEY*SIZEX   );


    printf(" ID %ld va da %ld a %ld \n", ID, Tstart, Tend);

    for(istart = Tstart; istart<Tend; istart+=NZs) {
      iend = min(istart +NZs, Tend);
      long int Rstart = max(istart-margin,0);
      long int Rend   = min(iend +margin ,SIZEZ);
      
 
      {
	int fd;
	size_t l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX  ; 

	omp_set_lock(&readlock_1);
	my_read(  absbone_outputfile   ,
		  abs_data          ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
	omp_unset_lock(&readlock_1);
	
      }
      { 
	int fd;
	long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;
	omp_set_lock(&readlock_2);
	my_read(  pagbone_outputfile   ,
		  pag_data          ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
	omp_unset_lock(&readlock_2);
      }


      {
	int fd;
	long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;

	omp_set_lock(&readlock_3);	
	my_read(  maskbone_outputfile   ,
		  mask_data          ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
	omp_unset_lock(&readlock_3);
	
      }

      long int cambi=1;
      int tour=0;
      while(cambi ) {
	
	// printf(" %d %d   cambi %d \n",  Rstart  ,  Rend  ,   cambi );
	
	cambi=0;
	tour++;
	long int iz,ix,iy;
	long int pos=0;

	long int ST_Y = SIZEX;
	long int ST_Z = SIZEX*SIZEY;

	for( iz=0; iz<(Rend-Rstart); iz++) {
	  double sum=0;
	  for( iy=0; iy<SIZEY; iy++) {
	    for( ix=0; ix<SIZEX; ix++) {
	      if( mask_data[pos] >0.0f ) {
		
		long int count=0;
		long int countnear=0;
		float sum=0;

		if(threshold==0) {

		  for(long int SZ= -(iz>0) ; SZ<=      (iz<(Rend-Rstart)-1) ;SZ++) {
		    for(long int SY= -(iy>0) ; SY<= (iy<(SIZEY-1));SY++) {
		      for(long int SX= -(ix>0) ; SX<= (ix<(SIZEX-1));SX++) {
			if( mask_data[pos+ST_Z*SZ + ST_Y*SY + SX] == -(tour-1)) {
			  count++;
			  sum+= pag_data[pos+ST_Z*SZ + ST_Y*SY + SX]; 
			  if( abs(SZ)+abs(SY)+abs(SX) ==1) countnear++;
			}
		      }
		    }
		  }
		  
		  if(count && countnear) {
		    output_data[pos] = abs_data[pos];
		    cambi++;
		    pag_data[pos]=sum/count;
		    mask_data[pos] = -tour;
		  }
		} else {
		  output_data[pos] = abs_data[pos];
		  pag_data[pos]=threshold;
		}
		
	      }
	      output_data[pos] = abs_data[pos];
	      sum+=  abs_data[pos]; 
	      pos++;
	    }
	  }
	  // printf(" slice %ld %e  cambi %d\n", iz+Rstart, sum, cambi);
	}
	printf("   cambi %d per ID %d\n",  cambi, ID);
      }
      printf(" lette tutte  per questo blocco %d\n", ID);
      for(long int i=0; i<(Rend-Rstart)*SIZEY*SIZEX;i++) {
	if(mask_data[i] ) {
	  output_data[i] = output_data[i]  -pag_data[i]; 
	  // output_data[i] = pag_data[i]; 
	} else {
	  output_data[i]=0.0f;
	}
      }

      if(0){
	struct flock fl;
	int fd;
	
	fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */

	fl.l_len    = sizeof(float)*(Rend-Rstart)*SIZEY*SIZEX; /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */


	fd = open( corrbone_outputfile , O_RDWR | O_CREAT , 640);
	if(fd==-1) {	
	  fprintf(stderr, " ERROR :c) could not open : %s  \n", corrbone_outputfile );
	  exit(0);
	}
	printf(" locking slices %d to %d   in file %s \n",istart, iend,  corrbone_outputfile );
	fl.l_start  =  ((long int) istart)*sizeof(float)*SIZEY*SIZEX; 
	fcntl(fd, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
	lseek( fd  ,fl.l_start , SEEK_SET);
	
	write(fd, output_data + SIZEY*SIZEX*(istart-Rstart) ,   sizeof(float)* (iend-istart)*SIZEY*SIZEX);
	fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
	fcntl(fd, F_SETLK, &fl); /* set the region to unlocked */
	// printf("unlocked \n");
	close(fd);
      } else {
	printf(" Scrivo per ID %d\n", ID);
	omp_set_lock(&writelock);
	my_write(corrbone_outputfile,
		 output_data + SIZEY*SIZEX*(istart-Rstart) ,
		 (size_t)   sizeof(float)* (iend-istart)*SIZEY*SIZEX ,
		 (off_t) ((long int) istart)*sizeof(float)*SIZEY*SIZEX ) ;

	omp_unset_lock(&writelock);

	printf(" Scrivo ID %d OK\n", ID);
	
      }
    }
    free(pag_data);
    free(abs_data);
    free(mask_data);
    free(output_data);
  }
  
  omp_destroy_lock(&writelock);
  omp_destroy_lock(&readlock_1);
  omp_destroy_lock(&readlock_2);
  omp_destroy_lock(&readlock_3);

}



void   incolla( char *pagbone_outputfile,
		char *maskbone_outputfile,
		char *corrected_outputfile,         
		char *outputfile,
		int myrank,
		int nprocs,
		int ncpus,
		long int MemPerProc, long int SIZEZ, long int SIZEY, long int SIZEX) {


  printf(" in c++ %d %d %d ncpus %d \n", SIZEZ,  SIZEY, SIZEX, ncpus);

  long int zPstep =  ((SIZEZ+nprocs-1)/nprocs);
  long int Pstart =   zPstep*myrank   ;
  long int Pend   =   min( Pstart + zPstep, SIZEZ   )        ;
  long int zTstep =  (((Pend-Pstart)+ncpus-1)/ncpus);

  omp_set_num_threads(ncpus);



  long int required_mem_per_thread = 4*sizeof(float)*(NZs)*SIZEY*SIZEX   ;
  int tot_cpus = ncpus;
  if(  required_mem_per_thread * ncpus * nprocs >  MemPerProc * ncpus *nprocs) {

    while( ncpus>0) {
      if (   required_mem_per_thread * ncpus * nprocs >  0.9*MemPerProc * tot_cpus *nprocs  ) {
	fprintf(stdout,"Not enough memory in routine incolla. Now reducing number of threads.Possibly think to reduce the slab size (hardcoded) or increase total memory \n");
	ncpus = ncpus -1 ; 
      } else {
	break;
      }
    }
    if( ncpus == 0 ) {
      fprintf(stderr,"Not enough memory in routine incolla. Think to reduce the slab size ( hardcoded), or increase total memory\n");
      exit(1);
    }
  }
  printf(" ncpus %d \n", ncpus);
  
#pragma omp  parallel
  {
    long int ID = omp_get_thread_num();
    // printf(" ID %d \n", ID);
    long int Tstart =   Pstart + zTstep*ID   ;
    long int Tend   =   min( Tstart + zTstep , Pend   )        ;

    long int istart, iend;
 
    float *pagbone_data    = (float *) malloc(sizeof(float)*(NZs)*SIZEY*SIZEX   );
    float *mask_data   = (float *) malloc(sizeof(float)*(NZs)*SIZEY*SIZEX   );
    float *corrected_data = (float *) malloc(sizeof(float)* NZs *SIZEY*SIZEX   );
    float *output_data = (float *) malloc(sizeof(float)* NZs *SIZEY*SIZEX   );

    for(istart = Tstart; istart<Tend; istart+=NZs) {
      iend = min(istart +NZs, Tend);
      long int Rstart = max(istart,0);
      long int Rend   = min(iend  ,SIZEZ);
       
      {

	
	int fd;
	long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;

	my_read(  pagbone_outputfile   ,
		  pagbone_data          ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
		
      }
      {
	int fd;
	long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;

	my_read(  maskbone_outputfile   ,
		  mask_data          ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
	
      }

      {
	int fd;
	long int l_start  =  ((long int)(Rstart ))*sizeof(float)*SIZEY*SIZEX ;

	my_read(   corrected_outputfile  ,
		    corrected_data        ,
		  (size_t) (  sizeof(float)* (Rend-Rstart)*SIZEY*SIZEX   ) ,
		  (off_t) l_start
		  );
      }


      for(long int i=0; i<(Rend-Rstart)*SIZEY*SIZEX;i++) {
	if(mask_data[i]) {
	  output_data[i] = max(  corrected_data[i], pagbone_data[i]*mask_data[i]    );
	} else {
	  output_data[i] =   corrected_data[i];
	}

	// if(mask_data[i]!=0.0f) {
	//   output_data[i] =  pagbone_data[i]; 
	// } else {
	//   output_data[i] =  corrected_data[i];
	// }

	// if(mask_data[i]) {
	//   output_data[i] =  pagbone_data[i]; 
	// } else {
	//   output_data[i] =  corrected_data[i];
	// }
      }
      
      
      if(0){
	struct flock fl;
	int fd;
	
	fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
	
	fl.l_len    = sizeof(float)*(Rend-Rstart)*SIZEY*SIZEX; /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */
	
	
	fd = open( outputfile , O_RDWR | O_CREAT , 640);
	if(fd==-1) {	
	  fprintf(stderr, " ERROR : could not open : %s  \n", outputfile );
	  exit(0);
	}
	// printf(" locking slices %d to %d   in file %s \n",istart, iend,  outputfile );
	fl.l_start  =  ((long int) istart)*sizeof(float)*SIZEY*SIZEX; 
	fcntl(fd, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
	lseek( fd  ,fl.l_start , SEEK_SET);
	write(fd, output_data  ,   sizeof(float)* (iend-istart)*SIZEY*SIZEX);
	fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
	fcntl(fd, F_SETLK, &fl); /* set the region to unlocked */
	// printf("unlocked \n");
	close(fd);
      } else {
	my_write(outputfile,
		 output_data,
		 (size_t) sizeof(float)* (iend-istart)*SIZEY*SIZEX,
		 (off_t) ((long int) istart)*sizeof(float)*SIZEY*SIZEX ) ;
	
      }
      
    }
    free(pagbone_data);
    free(mask_data);
    free(corrected_data);
    free(output_data);
  }
}
